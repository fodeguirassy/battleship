# Battleship, the game
The project AIMS to suggest an implementation of the [battleship](https://en.wikipedia.org/wiki/Battleship_(game)) with kotlin
Here are few steps to perform and run the project with IntelliJ IDEA

### IntelliJ IDEA global configuration
![](intellij_global_config.png)

### Import the project
Once cloned, please perform following steps to import the project

Click on import project

![](import_project.png)

Select the project and click open

![](finder_pick_project.png)

Make sure "Create project from existing sources is selected" and click next

![](create_from_existing.png)

Leave defaults values as they are on the next screen

![](next_screen_default_1.png)


The following dialog will show up. Click Yes

![](pick_yes.png)

Leave defaults and click Next

![](defaut_2.png)

Click Finish

![](default_3.png)


Next right click on the src folder. And set the sources root.

![](set_source.png)

Next configure Kotlin again

![](configure_Kotlin_again.png)

Open GridController class and click setup sdk

![](set_up_sdk.png)

Pick Java8 from the dialog and click OK

![](pick_java8.png)

'Kotlin not configured' message will show up. Click on 'configure'

![](configure_kotlin.png)

Pick 'Java'

![](pick_java.png)

Leave defaults. We recommend you make sure you've the latest Kotlin version bundled in IDEA. And click Ok

![](default_4.png)

Set the tests sources root

![](set_tests_sources_root.png)

Next, add Junit to the project classpath
Click file -> Project structure -> Libraries

![](project_structure.png)

Click on the plus button to add a new library

![](project_structure_plus.png)

Pick 'From Maven'

![](from_maven.png)

Type in junit in the search bar. Wait a moment for libraries to show up in the drop down list. Pick the right version. Check 'Download to' box. Then click 'OK'.

![](pick_junit.png)

Leave defaults and Click 'OK'

![](click_ok.png)

Click 'Apply' then 'OK'

![](apply_then_ok.png)


Open GridControllerTest class and launch tests.
