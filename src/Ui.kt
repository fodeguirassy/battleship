import java.util.*

fun main(args: Array<String>) {
    val controller = GridController()
    controller.setShips()
    controller.printList()
    controller.shoot(Random().nextInt(100))
    controller.printList()
    controller.check()
}