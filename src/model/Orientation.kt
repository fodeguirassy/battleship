package model

enum class Orientation {
    HORIZONTAL,
    VERTICAL
}