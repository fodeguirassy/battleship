package model

import java.util.*

class Ship(
        val type: ShipType,
        val orientation: Orientation = Orientation.values()[Random().nextInt(Orientation.values().size)]
) {
    var projection = mutableListOf<Int>()
}