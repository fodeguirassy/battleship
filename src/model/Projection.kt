package model

class Projection {
    var startIndex: Int = 0
    var hasRoom : Boolean = true
    var squareCount = 0
    var range = mutableListOf<Int>()
}