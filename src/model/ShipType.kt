package model
enum class ShipType(val squareCount: Int) {
    CARRIER(5),
    BattleShip(4),
    Cruiser(3),
    Submarine(3),
    Destroyer(2)
}