package builder.projection

import model.Projection

abstract class AbstractProjectionBuilder {

    internal var projection: Projection = Projection()

    fun setStartIndex(startIndex: Int) {
        projection.startIndex = startIndex
    }
    fun setSquareCount(squareCount: Int) {
        projection.squareCount = squareCount
    }

    abstract fun setRange()
    abstract fun setHasRoom()
}

