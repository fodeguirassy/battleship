package builder.projection

import builder.projection.AbstractProjectionBuilder

class HorizontalProjectionBuilder : AbstractProjectionBuilder() {
    override fun setRange() {
        val endIndex = (projection.startIndex + projection.squareCount) - 1 // upper bound is excluded
        for (i in projection.startIndex .. endIndex) {
            projection.range.add(i)
        }
    }

    override fun setHasRoom() {
        val rowIndex = projection.startIndex % 10 // initial row length
        projection.hasRoom = rowIndex + projection.squareCount < 9 // row length - 1 index starts at 0
    }
}