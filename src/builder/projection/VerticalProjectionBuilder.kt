package builder.projection

import GRID_COLUMNS
import builder.projection.AbstractProjectionBuilder

class VerticalProjectionBuilder : AbstractProjectionBuilder() {
    override fun setRange() {
        var columnIndex = projection.startIndex % GRID_COLUMNS
        val rangeCount = projection.squareCount - 1 // upper bound is excluded
        for(i in 0 .. rangeCount) {
            columnIndex += 10 // vertical range is not linear
            projection.range.add(columnIndex)
        }
    }

    override fun setHasRoom() {
        projection.hasRoom = projection.range.last() <= 99 // grid length - 1
    }
}