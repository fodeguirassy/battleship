package builder

import builder.projection.AbstractProjectionBuilder
import builder.projection.HorizontalProjectionBuilder
import builder.projection.VerticalProjectionBuilder
import model.Orientation
import model.Projection
import model.ShipType
import java.util.*

class ProjectionBuilderClient(
        private val index: Int,
        private val type: ShipType
) {

    private val orientation = Orientation.values()[Random().nextInt(Orientation.values().size)]
    private val projectionBuilder: AbstractProjectionBuilder

    init {
        projectionBuilder = when (orientation) {
            Orientation.HORIZONTAL -> HorizontalProjectionBuilder()
            Orientation.VERTICAL -> VerticalProjectionBuilder()
        }
    }

    fun build(): Projection {
        projectionBuilder.setStartIndex(index)
        projectionBuilder.setSquareCount(type.squareCount)
        projectionBuilder.setRange()
        projectionBuilder.setHasRoom()
        return projectionBuilder.projection
    }
}