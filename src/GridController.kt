import builder.ProjectionBuilderClient
import model.Orientation
import model.Projection
import model.Ship
import model.ShipType
import java.util.*

class GridController {

    private val gridList = mutableListOf<List<Int>>()
    val ships = mutableListOf<Ship>()
    private lateinit var projectionBuilderClient : ProjectionBuilderClient

    init {
        (0 until 100).toList().map {
            if (it % 10 == 0) {
                gridList += (it..(it + 9)).toList()
            }
        }
    }

    fun setShips() {
        ShipType.values().map {
            val ship = Ship(type = it)
            val projection = pickRange(ship.orientation, ship.type)
            ship.projection = projection
            ships.add(ship)
        }
    }

    private fun pickRange(orientation: Orientation, type: ShipType) : MutableList<Int> {
        val index = getRandomIndex()
        projectionBuilderClient = ProjectionBuilderClient(index, type)
        val projection = projectionBuilderClient.build()
        return if(!projection.isAvailable() || !projection.hasRoom) {
            pickRange(orientation, type)
        } else {
            projection.range
        }
    }

    private fun Projection.isAvailable() : Boolean {
        return ships.flatMap { it.projection }.none { it in this.range }
    }

    private fun getRandomIndex(): Int {
        return Random().nextInt(gridList.flatMap { it }.size)
    }

    fun shoot(squareIndex : Int) {
        ships.map { it.projection.removeIf { it == squareIndex } }
    }

    fun check() : Int {
        print("Left ships ${ships.count { it.projection.isNotEmpty() }}")
        return ships.count { it.projection.isNotEmpty() }
    }

    fun printList() {
        ships.map {
            println("Type : ${it.type} \t squares count : ${it.type.squareCount} \t projection : ${it.projection} remaining ${it.projection.size}")
        }
    }
}