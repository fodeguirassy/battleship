
import model.Orientation
import model.Ship
import model.ShipType
import org.junit.Before
import org.junit.Test
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class GridControllerTest {

    private val gridController = GridController()
    private lateinit var ships : MutableList<Ship>

    @Before
    fun setUp() {
        gridController.setShips()
        ships = gridController.ships
    }

    @Test
    fun setShipsTest() {
        assertEquals(5, ships.size)
        assertTrue { ships.all { it.type in ShipType.values() } }
        assertTrue { ships.all { it.projection.size == it.type.squareCount } }
    }

    @Test
    fun eachProjectionIsAvailable() {
        val randomProjection = ships[Random().nextInt(ships.size)]
        assertTrue { ships.flatMap { it.projection }
                .count { it in randomProjection.projection } == randomProjection.projection.size
        }
    }

    @Test
    fun eachHorizontalProjectionHasRoom() {
        assertTrue { ships.filter { it.orientation == Orientation.HORIZONTAL }
                .none {
                    it.projection.last() % GRID_COLUMNS > 9 // GRID ROWS minus 1 index starts at 0
                }
        }
    }

    @Test
    fun eachVerticalProjectionHasRoom() {
        assertTrue {
            ships.filter { it.orientation == Orientation.VERTICAL }
                    .none { it.projection.last() >= GRID_COLUMNS * GRID_ROWS }  // grid total length minus 1 index starts at 0
        }
    }

    @Test
    fun shootTest() {
        val carrierShip = ships.first()
        val randomSquare = carrierShip.projection[Random().nextInt(carrierShip.projection.size)]
        gridController.shoot(randomSquare)
        val initialSquareCount = carrierShip.type.squareCount
        assertTrue { carrierShip.projection.size == (initialSquareCount - 1) }
    }

    @Test
    fun checkTest() {
        val carrier = ships.first()
        val projection = carrier.projection
        (projection.first() .. projection.last())
                .filter { it in projection }
                .forEach { gridController.shoot(it) }
        assertEquals(gridController.check(), ships.size - 1)
    }
}